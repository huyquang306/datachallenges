<?php
class ExerciseString
{
    private $Check1;
    private $Check2;

    // Hàm đọc và ghi các thuộc tính
    public function getCheck1()
    {
        return $this->Check1;
    }

    // Set check1
    public function setCheck1($Check1)
    {
        $this->Check1 = $Check1;
    }

    // Get check2
    public function getCheck2()
    {
        return $this->Check2;
    }

    // Set check2
    public function setCheck2($Check2)
    {
        $this->Check2 = $Check2;
    }

    // Hàm đọc file
    public function readFile($fileName)
    {
        $file = @fopen($fileName,'r');
        $filesize = filesize($fileName);
        $data = fread($file,$filesize);
        fclose($file);

        return $data;
    }

    // Hàm kiểm tra chuỗi hợp lệ
    public function checkValidString($string)
    {
        return (strpos(" ".$string,"book") xor strpos(" ".$string,"restaurant"));
    }

    // Hàm ghi file   
    public function writeFile($check)
    {
        $file = @fopen('result_file.txt','w');
        fwrite($file,$check);
        fclose($file);
    }
}

// Hàm đếm số câu
function countSentence($data){
    $sentances = explode(".", $data);
    $sentances = array_filter(array_map('trim', $sentances));
    
    return count($sentances);
}

// Main 
$object1 = new ExerciseString();
$data1 = $object1->readFile('file1.txt');
$object1->setCheck1($object1->checkValidString($data1));
if($object1->getCheck1()){
    $string1 = "- check1 là chuỗi hợp lệ. ";
} else{
    $string1 = "- check1 là chuỗi không hợp lệ. ";
}

$data2 = $object1->readFile('file2.txt');
$object1->setCheck2($object1->checkValidString($data2));
if($object1->getCheck2()){
    $string2 = "- check2 là chuỗi hợp lệ . Chuỗi có ". countSentence($data2) ." câu.";
} else{
    $string2 = "- check2 là chuỗi không hợp lệ. ";
}

$object1->writeFile($string1."\n". $string2);
