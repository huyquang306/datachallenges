<?php

function normalize($str)
{
    $temp = explode(' ',$str);
    $temp = array_filter(array_map('trim',$temp));
    $result = implode(' ',$temp);

    return $result;
}

