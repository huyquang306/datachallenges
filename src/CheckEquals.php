<?php

function checkEquals($a, $b)
{
    if(count($a) !== count($b)) return false;

    for($i = 0; $i < count($a); $i++) {
        $flag = false;
        for($j = 0; $j < count($b); $j++) {
            if($a[$i] === $b[$j]) {
                $flag = true;
                array_splice($b, $j, 1);
                break;
            } 
        }

        if($flag === false) return false;
    }

    return true;
}

