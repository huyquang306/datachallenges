<?php

function integerToRoman($n)
{
    $int = [1000,900,500,400,100,90,50,40,10,9,5,4,1];
    $roman = ['M','CM','D','CD','C','XC','L','XL','X','IX','V','IV','I'];
    $result = "";

    if($n < 1 || $n > 3999) return "Error";

    for($i = 0; $i < 13; $i++) {
        while($n >= $int[$i]) {
            $result = $result.$roman[$i];
            $n = $n - $int[$i];
        }               
    }

    return $result;
}

