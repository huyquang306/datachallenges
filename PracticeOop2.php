<?php

// Abstract Class
abstract class Country
{
    protected $slogan;

    //hàm sayHello()
    abstract public function sayHello();

    // Hàm ghi các thuộc tính
    public function setSlogan($string)
    {
        $this->slogan = $string;
    }

    // Hàm đọc các thuộc tính
    public function getSlogan()
    {
        return $this->slogan;
    }
}

// Trait
trait Active
{
    // Hàm đưa ra tên class
    public function defindYourSelf()
    {
        return get_class($this);
    }
}

// Interface
interface Boss
{
    //Hàm kiểm tra tính hợp lệ của slogan
    public function checkValidSlogan();
}

class EnglandCountry extends Country implements Boss
{
    use Active;

    //Hàm sayHello() là người England in ra "Hello" 
    public function sayHello()
    {
        return "Hello";
    }

    // Check valid slogan
    public function checkValidSlogan()
    {
        return (strpos(" " . $this->slogan, "England") or strpos(" " . $this->slogan, "English"));
    }
}

class VietnamCountry extends Country implements Boss
{
    use Active;

    //Hàm sayHello() là người Việt Nam in ra "Xin chào" 
    public function sayHello()
    {
        return "Xin chao";
    }
    public function checkValidSlogan()
    {
        return (strpos(" " . $this->slogan, "Vietnam") and strpos(" " . $this->slogan, "hust"));
    }
}

// Main
$englandCountry = new EnglandCountry();
$vietnamCountry = new VietnamCountry();

$englandCountry->setSlogan('England is a country that is part of the United Kingdom. It shares land borders with Wales to the west and Scotland to the north. The Irish Sea lies west of England and the Celtic Sea to the southwest.');
$vietnamCountry->setSlogan('Vietnam is the easternmost country on the Indochina Peninsula. With an estimated 94.6 million inhabitants as of 2016, it is the 15th most populous country in the world .');

var_dump($englandCountry->sayHello()); // Hello
echo "<br>";
var_dump($vietnamCountry->sayHello()); // Xin chao
echo "<br>";
var_dump($englandCountry->checkValidSlogan()); // true
echo "<br>";
var_dump($vietnamCountry->checkValidSlogan()); // false

echo 'I am ' . $englandCountry->defindYourSelf();
echo "<br>";
echo 'I am ' . $vietnamCountry->defindYourSelf();
