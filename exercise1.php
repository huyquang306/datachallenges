<?php

//Bai 1
function checkValidString($str)
{
    if (strpos($str, 'book') !== false) $book = true;
    if (strpos($str, 'restaurant') !== false) $res = true;

    return $book or $res;
}

// Test bài 1
function showTest($str)
{
    echo "Chuỗi: \"" . $str . "\"<br/>";
    echo "Là chuỗi " . (checkValidString($str) ? "hợp lệ" : "không hợp lệ");
    echo "<br/><br/>";
}
showTest("chuỗi chứa Book");
showTest("chuỗi chứa book");
showTest("chuỗi chứa RESTAURANT");
showTest("chuỗi chứa book nhưng không chứa r e s ta u rant");
showTest("chuỗi chứa book và chứa restaurant");
showTest("chuỗi không chứa 2 thằng trên");
echo "<br/><br/>";

//Bai 2
//Hàm đếm số câu
function countSentences($str)
{
    $sentances = explode(".", $str);
    $sentances = array_filter(array_map('trim', $sentances));

    return count($sentances);
}

//Đọc file1.txt
$line1 = "";
$file1 = fopen("file1.txt", "r");
while (!feof($file1)) {
    $line1 .= fgetc($file1);
}

//Đọc file2.txt
$line2 = "";
$file2 = fopen('file2.txt', 'r');
while (!feof($file2)) {
    $line2 .= fgetc($file2);
}

//In ra kết quả
echo "Chuỗi: \"" . $line1 . "\"<br/>";
if (!checkValidString($line1)) {
    echo "Chuỗi không hợp lệ";
} else {
    echo "Chuỗi hợp lệ. Chuỗi bao gồm" . countSentences($line1) . " câu. \n";
}

echo "<br/><br/>";
echo "Chuỗi: \"" . $line2 . "\"<br/>";
if (!checkValidString($line2)) {
    echo "Chuỗi không hợp lệ";
} else {
    echo "Chuỗi hợp lệ. Chuỗi bao gồm" . countSentences($line2) . " câu. \n";
}
